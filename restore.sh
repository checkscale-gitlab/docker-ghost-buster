#!/usr/bin/env bash
set -e
source .env

docker-compose exec backup restore -i
